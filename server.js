var express = require("express");
var bodyParser = require("body-parser");
const port = 8080;

var app = express();

const { Random } = require("random-js");
const random = new Random();
const value = random.integer(1, 10);
console.log("Chut... voici le nombre mystère : " + value);


app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/", function(req, res) {
    res.sendFile("index.html"); // ou sendFile
})

app.get("/nouvelle_chance/", function(req, res) {
    res.sendFile(__dirname + '/public/nouvelle_chance.html');
})

app.post("/reponse/", function(req, res) {
    var reponse = req.body.nombre;
    if (reponse == value) {
        console.log("Gagné !");
        res.send("Gagné")
    }
    else {
        console.log("Perdu !");
        res.redirect("/nouvelle_chance/");
        }
})



app.listen(port, () => console.log(`Je suis lancé ! ${port}`));
